const mongoose = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;
const usersSchema = new Schema(
  {
    name: {
      type: String
    },
    image: {
      type: String
    },
  },
  {
    timestamps: true
  }
);
usersSchema.plugin(paginate);

let Users = mongoose.model('chat_stream_users', usersSchema);
module.exports = Users;