const mongoose = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;
const chatSchema = new Schema(
  {
    text: {
      type: String
    },
    chat_list_id: {
      type: Schema.Types.ObjectId,
      ref: 'chat_stream_chat_room'
    },
    sender: {
      type: Schema.Types.ObjectId,
      ref: 'chat_stream_users'
    }
  },
  {
    timestamps: true
  }
);
chatSchema.plugin(paginate);

let Chat = mongoose.model('chat_stream_chat', chatSchema);
module.exports = Chat;