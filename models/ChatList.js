const mongoose = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;
const ChatRoomSchema = new Schema(
  {
    chat_type: {
      type: String,
      enum: ['user-user', 'user-multi-user', 'user-group'],
      default: 'user-multi-user'
    },
    receivers: [{
      type: Schema.Types.ObjectId,
      ref: 'chat_stream_users'
    }],
    created_by: {
      type: Schema.Types.ObjectId,
      ref: 'chat_stream_users'
    },
    name: {
      type: String
    },
    image: {
      type: String
    },
  },
  {
    timestamps: true
  }
);

ChatRoomSchema.plugin(paginate);
let ChatRoom = mongoose.model('chat_stream_chat_list', ChatRoomSchema);
module.exports = ChatRoom;