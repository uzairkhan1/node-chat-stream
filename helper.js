const Chat = require('./models/Chat');
const ChatList = require('./models/ChatList');

const Helper = {
  sendResponse: (res, data) => {
    res.statusCode = 200;
    res.json({ data: data });
  },
  sendNotFoundResponse: (res, name) => {
    res.statusCode = 200;
    res.json({ data: [], error: 'Not found any ' + name + '!' });
  },
  errorResponse: (res, msg) => {
    res.statusCode = 400;
    res.json({ status: 'failed', error: msg });
  },
  messageResponse: (res, message) => {
    res.statusCode = 200;
    res.json({ status: 'success', message });
  },
  addChat: (chat) => {
    let chatMessage = new Chat({ text: chat.text, sender: chat.sender, chat_list_id: chat.chat_list_id });
    chatMessage.save();
  },
  sendPaginationResponse: (res, records, params = {}) => {
    res.statusCode = 200;
    let response = {
      ...params,
      data: records.docs,
      totalDocs: records.totalDocs,
      limit: records.limit,
      page: records.page,
      totalPages: records.totalPages,
      pagingCounter: records.pagingCounter,
      hasPrevPage: records.hasPrevPage,
      hasNextPage: records.hasNextPage,
      prevPage: records.prevPage,
      nextPage: records.nextPage
    }
    res.json(response);
  },
  sendMultiUserMsg: (room, clients, event) => {
    let allIds = room.receivers;
    allIds.splice(allIds.indexOf(event.sender), 1);// removed sender
    allIds.forEach((item) => {
      if (item && clients[item]) {
        let channelName = room.chat_type == 'user-group' ? 'group-message' : 'multi-user-message'
        clients[item].forEach(cli => cli.emit(channelName, event));
      }
    });
  },
  userToUserChat: async (event) => {
    let sender = event.sender;
    let receiver = event.receiver;
    let query = {}
    if (sender && receiver) {
      query = Helper.userToUserQuery(sender, receiver)
      let chat_list = await ChatList.findOne(query);
      if (!chat_list) {
        let data = { chat_type: 'user-user', created_by: sender, receivers: [receiver] };
        chat_list = new ChatList(data);
        chat_list.save();
      }
      event.chat_list_id = chat_list._id;
      Helper.addChat(event);
    }

  },
  userToUserQuery: (sender, receiver) => {
    return query = { $or: [{ chat_type: 'user-user', created_by: sender, receivers: [receiver] }, { chat_type: 'user-user', created_by: receiver, receivers: [sender] }] }
  }
};
module.exports = Helper;