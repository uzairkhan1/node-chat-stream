const Connect = require('./config/db.js');
const SocketIOFileUpload = require('socketio-file-upload');
//Routes
const userRoutes = require('./routes/UserRoutes');
const chatListRoutes = require('./routes/ChatList');
const uploadRoutes = require('./routes/Upload');


const Socket_IO = require('./socket.js');
let { ChatConfig, SetChatConfig } = require('./config/index.js');


const NodeChatCtream = (socket, app) => {
  if (ChatConfig.db_url) {
    Connect(ChatConfig.db_url).then(db => console.log("Chat library DB connected correctly to the server"));
  }
  else throw new Error("Databse URL not found.");

  //routes
  app.use("/chat-stream/users", userRoutes);
  app.use("/chat-stream/chatList", chatListRoutes);
  app.use("/chat-stream/upload", uploadRoutes);
  app.use(SocketIOFileUpload.router)

  Socket_IO(socket);

}



module.exports = {
  SetChatConfig: SetChatConfig,
  StartChat: NodeChatCtream
}