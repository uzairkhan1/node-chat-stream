const express = require('express');
let path = require('path');
const __dirname_ = path.resolve();
const Upload = express.Router();
const fs = require("fs");

Upload.route("/*").get(async (req, res, next) => {
  let url = __dirname_ + '/uploads' + req.path;
  if (fs.existsSync(url)) {
    res.sendFile(url);
  }
  else {
    res.statusCode = 400;
    res.json({ status: 'failed', error: "File not found!" });
  }
});

module.exports = Upload;