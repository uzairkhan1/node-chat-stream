let mongoose = require('mongoose');
let bluebird = require('bluebird');

mongoose.Promise = bluebird;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
const Connect = (url) => mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

module.exports = Connect;