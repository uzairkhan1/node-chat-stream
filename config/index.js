var ChatConfig = { db_url: '', port: 6000, db_prefix: '' };
var SetChatConfig = (config) => {
  ChatConfig.db_url = config.db_url ? config.db_url : '';
}
module.exports = {
  ChatConfig,
  SetChatConfig
};